#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `frkl_events` package."""

import frkl.events
import pytest  # noqa


def test_assert():

    assert frkl.events.get_version() is not None
