# Usage


## Getting help

To get information for the `events` command, use the ``--help`` flag:

{{ cli("events", "--help") }}
