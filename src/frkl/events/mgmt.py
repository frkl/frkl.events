# -*- coding: utf-8 -*-

"""Main module."""
import asyncio
import atexit
import logging
import threading
from threading import Thread
from typing import TYPE_CHECKING, Dict, Iterable, Optional, Union

import anyio
from anyio import create_task_group
from frkl.common.exceptions import FrklException
from frkl.common.types import isinstance_or_subclass
from frkl.events.event import Event
from frkl.events.handlers import EventSource, EventTarget


log = logging.getLogger("frkl")

if TYPE_CHECKING:
    pass


class EventManagement(object):
    """The central class of this library, connects sources to targets and kicks off the event monitoring.

    Args:

        source (frkl.events.handlers.EventSource): the source object that emits 'CheckResult' objects
        targets (Iterable[frkl.events.handlers.EventTarget]): a list of target objects that consume the 'CheckResults'
    """

    def __init__(
        self,
        sources: Union[EventSource, Iterable[EventSource]],
        targets: Union[EventTarget, Iterable[EventTarget]],
    ):

        self._sources: Dict[str, EventSource] = {}
        if isinstance_or_subclass(sources, EventSource):
            sources = [sources]  # type: ignore
        for _s in sources:  # type: ignore
            if _s.id in self._sources.keys():
                raise FrklException(
                    msg=f"Can't create source with id '{_s.id}'",
                    reason="Duplicate id.",
                )
            self._sources[_s.id] = _s
        self._targets: Dict[str, EventTarget] = {}
        if isinstance_or_subclass(targets, EventTarget):
            targets = [targets]  # type: ignore
        for _t in targets:  # type: ignore
            if _t.id in self._targets.keys():
                raise FrklException(
                    msg=f"Can't create target with id '{_t.id}'.",
                    reason="Duplicate id.",
                )
            self._targets[_t.id] = _t

        self._thread: Optional[Thread] = None
        self._stop_timeout: float = 2.0

        self._start_thread_as_daemon: bool = True

    async def _connect(self):
        """Connect the source and all targets."""

        failed: Dict[EventTarget, Exception] = {}

        for _source in self._sources.values():
            log.debug(f"Trying to connect to source: {_source.id}")
            try:
                await _source.connect_source()
                log.debug("Source connected.")
            except Exception as e:
                failed[_source] = e

        if failed:
            if len(failed) == 1:
                _fs = list(failed.keys())[0]
                _fe = failed[_fs]
                msg = f"Can't connect to source '{_fs.id}'."
                reason = str(_fe)
            else:
                failed_ids = [_fs.id for _fs in failed.keys()]
                msg = f"Can't connect to sources: {', '.join(failed_ids)}"
                _reason = []
                for _fs, _fe in failed.items():
                    _reason.append(f"  [bold]{_fs.id}[/bold]:")
                    _reason.append(f"       {_fe}")
                reason = "\n".join(_reason)
            raise FrklException(msg=msg, reason=reason)

        for _target in self._targets.values():
            log.debug(f"Trying to connect to target: {_target.id}...")
            try:
                await _target.connect()
                log.debug("Target connected.")
            except Exception as e:
                failed[_target] = e
        if failed:
            if len(failed) == 1:
                _ft = list(failed.keys())[0]
                _fe = failed[_ft]
                msg = f"Can't connect to target '{_ft.id}'."
                reason = str(_fe)
            else:
                failed_ids = [_ft.id for _ft in failed.keys()]
                msg = f"Can't connect to targets: {', '.join(failed_ids)}"
                _reason = []
                for _ft, _fe in failed.items():
                    _reason.append(f"  [bold]{_ft.id}[/bold]:")
                    _reason.append(f"       {_fe}")
                reason = "\n".join(_reason)
            raise FrklException(msg=msg, reason=reason)

    async def _disconnect(self):
        """Disconnect the source and all targets"""

        for _source in self._sources.values():
            try:
                log.debug(f"Disconnecting source {_source.id}...")
                await _source.disconnect_source()
                log.debug("Source disconnected.")
            except Exception as e:
                log.warning(f"Failed to disconnect source '{_source.id}': {e}")

        for _target in self._targets.values():

            try:
                log.debug(f"Disconnecting target {_target.id}...")
                await _target.disconnect()
                log.debug("Target disconnected.")
            except Exception as e:
                log.warning(f"Failed to disconnect target '{_target.id}': {e}")

    def start(self) -> None:
        """Start the check/listen process."""

        self._stop_event = threading.Event()

        def wrap(stop_event: threading.Event):

            log.debug("Starting events pipeline...")

            async def watch():
                async def watch_source(_source):

                    log.debug(f"Starting source: {_source.id}")

                    # watching for events until stop event is set
                    async for _event in _source.start_source():
                        await self.write_result(_event)

                async def stop_source(_source):

                    log.debug(f"Stopping source: {_source.id}")

                    # stop source
                    rest_events: Optional[Iterable[Event]] = await source.stop_source()
                    if rest_events:
                        for _event in rest_events:
                            await self.write_result(_event)

                        # disconnect source
                        await source.disconnect_source()

                    log.debug(f"Source '{_source.id}' stopped and disconnected.")

                # connecting all sources
                await self._connect()

                # watch for events
                async with create_task_group() as tg:
                    for source in self._sources.values():
                        await tg.spawn(watch_source, source)

                    while True:
                        if stop_event.is_set():
                            log.debug("Stop event set, stopping event monitoring...")
                            break
                        await anyio.sleep(0.1)
                    await tg.cancel_scope.cancel()

                # stopping all sources
                async with create_task_group() as tg:
                    for source in self._sources.values():
                        await tg.spawn(stop_source, source)

                log.debug("Disconnecting sources and targets.")
                await self._disconnect()
                log.debug("Events pipeline stopped...")
                return

            asyncio.run(watch())
            log.debug("")

        self._thread = Thread(
            target=wrap, args=(self._stop_event,), daemon=self._start_thread_as_daemon
        )

        self._thread.start()

        def stop_monitoring():
            log.debug("Stopping event monitoring...")
            self.stop(timeout=self._stop_timeout)

        atexit.register(stop_monitoring)

    def stop(self, timeout: Optional[float] = None):

        if self._thread is None:
            return

        if not self._thread.is_alive():
            return

        self._stop_event.set()
        if timeout is None:
            timeout = self._stop_timeout
        self._thread.join(timeout=timeout)

    async def write_result(self, event: Event) -> None:
        """Write result to all targets."""

        if not self._targets:
            return

        if not isinstance_or_subclass(event, Event):
            raise Exception(
                f"Invalid type '{type(event)}' for event (should be 'Event' or a subclass of it). This is a bug."
            )

        if len(self._targets) == 1:
            target = list(self._targets.values())[0]
            # log.debug(f"Write event to target: {target.id}")
            try:
                await target.write_events(event)
            except Exception as e:
                log.error(
                    f"Can't write event to target '{target.id}': {e}", exc_info=True
                )

            return

        async def wrap(_result: Event, _target: EventTarget):

            # log.debug(f"Write event to target: {_target.id}")
            try:
                await _target.write_events(_result)
                log.debug(f"Finished writing to target: {_target.id}")
            except Exception as e:
                log.error(
                    f"Can't write event to target '{_target.id}': {e}", exc_info=True
                )

        async with create_task_group() as tg:

            for _t in self._targets.values():
                await tg.spawn(wrap, event, _t)
