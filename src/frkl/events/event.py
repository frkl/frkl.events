# -*- coding: utf-8 -*-
import json
import os
from abc import ABCMeta, abstractmethod
from typing import Any, Mapping, Optional

from fastavro import parse_schema
from frkl.events.defaults import FRKL_EVENTS_RESOURCES_FOLDER


class Event(metaclass=ABCMeta):
    @abstractmethod
    def get_data(self) -> Any:
        pass

    def get_metadata(self) -> Optional[Mapping[str, Any]]:
        return None


class TestEvent(Event):
    def __init__(self, path: str):

        self._path = path

        schema_file = os.path.join(FRKL_EVENTS_RESOURCES_FOLDER, "test.avsc")
        with open(schema_file, "r") as f:
            content = json.load(f)
        self._schema = parse_schema(content)

    def get_metadata(self) -> Mapping[str, Any]:
        return {"schema": self._schema, "schema_type": "avro"}

    def get_data(self) -> Any:

        return {"url": f"http://frkl.io/{self._path}"}
