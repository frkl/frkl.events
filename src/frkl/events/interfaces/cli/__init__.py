# -*- coding: utf-8 -*-
import time

import asyncclick as click
from frkl.events.app_events.mgmt import AppEventManagement
from frkl.events.event import TestEvent


try:
    import uvloop

    uvloop.install()
except Exception:
    pass

click.anyio_backend = "asyncio"


@click.command()
@click.pass_context
async def cli(ctx):

    # event = TestEvent()
    # import pp
    # schema = event.get_schema()
    #
    # records = [
    #     {"url": "https://frkl.io"},
    #     {"url": "https://freckles.io"}
    # ]
    #
    # with open('/tmp/markus', 'wb') as out:
    #     fastavro.writer(out, schema, records)

    # source = QueueSource()
    # target = TerminalTarget()
    # mgmt = EventManagement(sources=[source], targets=[target])
    #
    # await mgmt.connect()
    #
    # async with create_task_group() as tg:
    #     await tg.spawn(mgmt.start)
    #     event = TestEvent("/path_1")
    #     await source.add_event(event)
    #     event = TestEvent("/path_2")
    #     await source.add_event(event)
    #
    # print("XXXXX")

    # hpm = EventFactory()
    # import pp
    # pp(hpm.available_sources)
    # pp(hpm.available_targets)
    #
    # em = hpm.create_event_management(sources="dummy_source", targets="terminal_target")
    #
    # await em.connect()
    #
    # await em.start()

    aem = AppEventManagement(target_configs="terminal_target")

    # await aem._events_management.connect()

    aem.start_monitoring()
    aem.add_app_event(TestEvent("https://frkl.io"))

    time.sleep(4)
    aem.add_app_event(TestEvent("https://frkl.io"))

    # def wrap():
    #     aem._events_management.stop()
    #     aem._events_management._thread.join(0.5)

    # atexit.register(wrap)
    # aem._events_management.stop()
    aem.stop_monitoring()


if __name__ == "__main__":
    cli()
