# -*- coding: utf-8 -*-
from typing import Any, Mapping, Optional

from frkl.common.cli import get_console
from frkl.common.environment import get_var_value_from_env
from frkl.common.formats.serialize import make_serializable, to_value_string
from frkl.common.types import isinstance_or_subclass
from frkl.events.app_events import MsgEvent, ResultEvent, TaskEvent
from frkl.events.event import Event
from frkl.events.handlers import EventTarget
from frkl.explain.explanation import Explanation
from frkl.tasks.task_desc import TaskDesc
from frkl.tasks.task_watchers.rich import TasksProgress
from rich.console import Console


CONSOLE_HTML_FORMAT = "{code}"


class RichTerminalTarget(EventTarget):
    """Simple target to print check results to the terminal."""

    _plugin_name = "terminal"

    def __init__(self, **config):
        self._config = config  # ignored for now
        super().__init__()

        self._started_tasks = 0
        self._finished_tasks = 0
        self._console: Console = config.get("console", get_console())

        self._finished_max_priority: int = config.get("finished_max_priority", 30)

        export_html = get_var_value_from_env("export_html")

        if "export_html" not in self._config.keys():
            self._export_html = export_html and export_html.lower() == "true"
        else:
            self._export_html = self._config["export_html"]

        if self._export_html:
            self._console.record = True

        self._progress: TasksProgress = TasksProgress(
            finished_max_priority=self._finished_max_priority, console=self._console
        )
        # self._task: TaskID = self._progress.add_task(
        #     "starting task", start=False, total=10
        # )

        self._progress_started: bool = False

    def get_id(self) -> str:
        return "terminal"

    @property
    def current_open_tasks(self) -> int:

        return self._started_tasks - self._finished_tasks

    async def write_events(self, *events: Event) -> None:

        for event in events:

            if isinstance(event, TaskEvent):

                source = event._source
                event_details = event._event_details
                event_name = event._event_name

                if event_name == "task_started":
                    self._started_tasks += 1
                    self._task_started(source, event_details)
                elif event_name == "task_finished":
                    self._finished_tasks += 1
                    self._task_finished(source, event_details)

                elif event_name == "task_failed":
                    self._finished_tasks += 1
                    self._task_failed(source, event_details)

            elif isinstance_or_subclass(event, MsgEvent):
                msg = event.msg  # type: ignore
                if msg.startswith("\n"):
                    self._console.line()
                get_console().print(msg)
                if msg.endswith("\n"):
                    self._console.line()
            elif isinstance_or_subclass(event, ResultEvent):
                self._console.print("[title]Result:[/title]")
                self._console.line()
                self._console.print(event.get_data())
                self._console.line()
            else:
                data = event.get_data()

                if isinstance_or_subclass(data, Explanation):
                    self._console.print(data)
                else:
                    serialized = make_serializable(data)
                    value_string = to_value_string(serialized)
                    self._console.print(value_string)

            # else:
            #     raise TypeError(f"Invalid event type in terminal target: {type(event)}")
            #

            # print("===========================")
            # print(event._event_name)
            # print(event._event_details)
            # print(event._source)

    @property
    def progress(self) -> TasksProgress:

        return self._progress

    def _task_started(self, source: TaskDesc, event_details: Mapping[str, Any]):

        if not self._progress_started and self.current_open_tasks > 0:
            self.progress.start()
            self._progress_started = True

        # if source == self._root_task:
        #     parent_id: Optional[str] = None
        if source.parent:
            parent_id: Optional[str] = source.parent.id
        else:
            parent_id = None

        self.progress.task_started(source, parent_id)

    def _task_finished(self, source: TaskDesc, event_details: Mapping[str, Any]):

        self.progress.task_finished(source)

        if self.current_open_tasks == 0:
            self._progress.stop()
            self._progress.refresh()
            renderables = self._progress.get_renderables()
            for r in renderables:
                self._console.print(r)
            self._console.line()

    def _task_failed(self, source: TaskDesc, event_details: Mapping[str, Any]):

        self.progress.task_failed(source)

        if self.current_open_tasks == 0:
            self._progress.stop()
            renderables = self._progress.get_renderables()
            for r in renderables:
                self._console.print(r)
            self._console.line()

    async def disconnect(self) -> None:
        """Disconnect the target."""

        if self._export_html:
            html = self._console.export_html(
                inline_styles=True, code_format=CONSOLE_HTML_FORMAT
            )
            print("# HTML_START")
            print(html)
            print("# HTML_END")
