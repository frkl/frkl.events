# -*- coding: utf-8 -*-
import atexit
import logging
import os
import time
from typing import Any, AsyncIterator, Dict, Iterable, List, Mapping, Optional, Union

import janus
from frkl.common.cli import get_console
from frkl.common.exceptions import FrklException
from frkl.common.filesystem import ensure_folder
from frkl.common.iterables import ensure_iterable
from frkl.common.types import isinstance_or_subclass
from frkl.events.app_events import TaskEvent, auto_create_event
from frkl.events.app_events.log import LogTarget
from frkl.events.event import Event
from frkl.events.handlers import EventSource
from frkl.events.mgmt import EventManagement
from frkl.events.plugins import EventFactory
from frkl.types.typistry import Typistry
from pubsub import pub


def get_log_handlers(*configs: Union[str, Mapping[str, Any]], typistry: Typistry):

    # root = logging.getLogger()
    # logging_level = logging.DEBUG
    # handler: logging.Handler = None

    plugin_manager = typistry.get_plugin_manager(logging.Handler)

    handlers: List[logging.Handler] = []
    for config in configs:

        if isinstance(config, str):
            _config: Dict[str, Any] = {"type": config}
        else:
            _config = dict(config)

        handler_type = _config.pop("type", None)

        level = _config.pop("level", logging.INFO)
        format = _config.pop("format", None)
        formatter = None
        if format:
            formatter = logging.Formatter(format)

        if handler_type is None:
            raise FrklException(
                f"Can't create logger for configuration: {_config}",
                reason="No config 'type' key present.",
            )

        plugin = plugin_manager.get_plugin(handler_type)
        if plugin is None:
            raise FrklException(
                f"Can't create logger of type: {handler_type}",
                reason="No such log handler available.",
            )

        plugin_obj: logging.Handler = plugin(**_config)
        if formatter:
            plugin_obj.setFormatter(formatter)
        plugin_obj.setLevel(level)

        handlers.append(plugin_obj)

    return handlers

    # if handler == "file":
    #
    #     formatter = logging.Formatter('%(levelname)s - %(name)s: %(message)s')
    #     fh = logging.FileHandler('/tmp/bring.log')
    #     fh.setLevel(logging_level)
    #     fh.setFormatter(formatter)
    #
    #     root.addHandler(fh)
    #
    # elif handler == "rich":
    #
    #     rh = RichHandler(show_time=False, console=self._console, level=self._logging_level)
    #     root.addHandler(rh)


class AppEventManagement(EventSource):
    def __init__(
        self,
        base_topic: str,
        target_configs: Union[
            str, Mapping[str, Any], Iterable[Union[str, Mapping[str, Any]]]
        ],
        typistry: Optional[Typistry] = None,
        events_subtopic: Optional[str] = None,
        logger_name: Optional[str] = None,
        event_log_level: int = logging.INFO,
        root_log_level: int = logging.DEBUG,
        log_file: Optional[str] = None,
        terminal_log: Any = None,
    ):

        self._base_topic: str = base_topic
        if typistry is None:
            typistry = Typistry()
        self._typistry: Typistry = typistry
        self._event_factory: EventFactory = EventFactory(typistry=typistry)
        self._queue: Optional[janus.Queue] = None
        self._monitoring_started: bool = False

        self._logger_name: Optional[str] = logger_name
        self._event_log_level: int = event_log_level
        self._root_log_level: int = root_log_level
        self._log_file: Optional[str] = log_file
        self._log_target = LogTarget(logger_name=self._logger_name)
        if terminal_log is True:
            terminal_log = logging.INFO
        self._terminal_log: Union[Any] = terminal_log

        self._shutdown_timeout: float = 4

        handler_configs = []

        if self._log_file:

            if os.path.isdir(self._log_file):
                filename = time.strftime("%Y%m%d-%H%M%S") + ".log"
            else:
                filename = self._log_file
            ensure_folder(os.path.dirname(self._log_file), mode=0o700)
            file_handler = {
                "type": "file_handler",
                "filename": filename,
                "format": "%(levelname)s - %(name)s: %(message)s",
                "mode": "w",
                "level": logging.DEBUG,
            }
            handler_configs.append(file_handler)

        if self._terminal_log:

            rich_handler = {
                "type": "rich_handler",
                "show_time": False,
                "console": get_console(),
                "level": self._terminal_log,
            }
            handler_configs.append(rich_handler)

        log_handlers = get_log_handlers(*handler_configs, typistry=self._typistry)
        logging.basicConfig(
            level=self._root_log_level,
            format="%(name)s: %(message)s",
            datefmt="[%X]",
            handlers=log_handlers,
        )

        self._started_tasks: int = 0

        super().__init__()

        targets: List[Any] = ensure_iterable(target_configs, always_create_new_obj=True)  # type: ignore
        targets.append(self._log_target)

        self._events_management: EventManagement = (
            self._event_factory.create_event_management(sources=self, targets=targets)
        )

        if events_subtopic:
            pub.subscribe(self.task_handler, f"{self._base_topic}.{events_subtopic}")
        else:
            pub.subscribe(self.task_handler, f"{self._base_topic}")

    def task_handler(
        self, event_name: str, source: Any, event_details: Mapping[str, Any]
    ):

        task_event = TaskEvent(
            event_name=event_name, source=source, event_details=event_details
        )
        self.add_app_event(task_event)

    def add_app_event(self, event: Any):

        if self._queue is None and not self._monitoring_started:
            self.start_monitoring()

        while self._queue is None:
            time.sleep(0.01)

        queue = self._queue.sync_q

        if not isinstance_or_subclass(event, Event):
            event = auto_create_event(event)

        queue.put(event)

    def start_monitoring(self) -> None:

        if self._monitoring_started:
            return

        self._monitoring_started = True
        self._events_management.start()
        atexit.register(self.stop_monitoring)

    def stop_monitoring(self, timeout: Optional[float] = None) -> None:

        if self._monitoring_started:
            if timeout is None:
                timeout = self._shutdown_timeout
            self._events_management.stop(timeout=timeout)
            self._monitoring_started = False

    async def start_source(self) -> AsyncIterator[Event]:
        self._queue = janus.Queue()
        queue = self._queue.async_q
        while True:
            event = await queue.get()
            yield event
