# -*- coding: utf-8 -*-
from frkl.common.cli import get_console
from frkl.events.event import Event
from frkl.events.handlers import EventTarget
from rich.console import Console


class PlainTerminalTarget(EventTarget):

    _plugin_name = "plain"

    def __init__(self, **config):

        self._config = config
        self._console: Console = config.get("console", get_console())
        super().__init__()

    def get_id(self):

        return "plain"

    async def write_events(self, *events: Event) -> None:

        for event in events:
            self._console.print(str(event))
