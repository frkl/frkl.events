# -*- coding: utf-8 -*-
import logging
from typing import Optional

from frkl.events.event import Event
from frkl.events.handlers import EventTarget


class LogTarget(EventTarget):

    _plugin_name = "log"

    def __init__(
        self, logger_name: Optional[str] = None, log_level: int = logging.INFO, *config,
    ):

        self._config = config

        self._logger_name: Optional[str] = logger_name
        self._logging_level: int = log_level

        self._log = logging.getLogger(name=self._logger_name)

        super().__init__()

    def get_id(self):

        logger_name = self._logger_name
        if logger_name is None:
            logger_name = "root"

        return f"logger_{logger_name}"

    async def write_events(self, *events: Event) -> None:

        for event in events:
            self._log.log(self._logging_level, str(event))
