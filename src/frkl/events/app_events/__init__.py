# -*- coding: utf-8 -*-
from typing import Any, Mapping

from frkl.common.formats.serialize import make_serializable, to_value_string
from frkl.common.strings import reindent
from frkl.common.types import isinstance_or_subclass
from frkl.events.event import Event
from frkl.explain.explanations.exception import ExceptionExplanation
from frkl.tasks.task_desc import TaskDesc


def auto_create_event(data: Any) -> "Event":

    if isinstance_or_subclass(data, Exception):
        return ExceptionEvent(data)
    elif isinstance(data, str):
        return MsgEvent(msg=data)
    else:
        return InfoEvent(data)


class TaskEvent(Event):
    def __init__(self, event_name: str, source: Any, event_details: Mapping[str, Any]):

        self._event_name: str = event_name
        self._source: Any = source
        self._event_details: Mapping[str, Any] = event_details

    def get_data(self):

        return {
            "event_name": self._event_name,
            "source": self._source,
            "event_details": self._event_details,
        }

    def __str__(self):
        data = self.get_data()

        source: TaskDesc = data["source"]
        return f"{data['event_name']}: {source.msg}"


class MsgEvent(Event):
    def __init__(self, msg: str):

        self._msg: str = msg

    @property
    def msg(self) -> str:
        return self._msg

    def get_data(self) -> Any:

        return self._msg

    def __str__(self):

        return self._msg


class ResultEvent(Event):
    def __init__(self, data: Any):

        self._data = data

    def get_data(self) -> Any:

        return self._data

    def __str__(self):

        data_str = reindent(to_value_string(make_serializable(self.get_data())), 2)
        return f"Result:\n{data_str}"


class InfoEvent(Event):
    def __init__(self, data: Any):

        self._data: Any = data

    def get_data(self) -> Any:

        return self._data

    def __str__(self):

        data_str = reindent(to_value_string(make_serializable(self.get_data())), 2)
        return f"Info:\n{data_str}"


class ExceptionEvent(Event):
    def __init__(self, exception: Exception):

        self._exception: Exception = exception

    def get_data(self):

        return ExceptionExplanation(self._exception)
