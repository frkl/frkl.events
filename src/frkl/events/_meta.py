# -*- coding: utf-8 -*-
from typing import Any, Dict


project_name = "frkl.events"
exe_name = "events"
project_main_module = "frkl.events"
project_slug = "frkl_events"

pyinstaller: Dict[str, Any] = {}
