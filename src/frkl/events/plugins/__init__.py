# -*- coding: utf-8 -*-
import collections
import os
from enum import Enum
from pathlib import Path
from typing import Any, Iterable, List, Mapping, Optional, Union

from frkl.common.exceptions import FrklException
from frkl.common.iterables import ensure_iterable
from frkl.common.types import isinstance_or_subclass
from frkl.events.handlers import EventHandler, EventSource, EventTarget
from frkl.events.mgmt import EventManagement
from frkl.types.plugins import PluginManager
from frkl.types.typistry import Typistry
from ruamel.yaml import YAML


class HANDLER_TYPE(Enum):

    source = 0
    target = 1


class EventFactory(object):
    def __init__(self, typistry: Optional[Typistry] = None):

        if typistry is None:
            typistry = Typistry()

        self._typistry = typistry
        self._source_plugin_manager: PluginManager = typistry.get_plugin_manager(
            EventSource
        )
        self._target_plugin_manager: PluginManager = typistry.get_plugin_manager(
            EventTarget
        )

    def _read_config_file(self, path: Union[str, Path]) -> Mapping[str, Any]:
        """Create a target from a (yaml) file.

        Args:
            path: the path to the target config

        Returns:
            EventTarget: the target object
        """
        if isinstance(path, str):
            path = Path(os.path.expanduser(path))

        if not path.exists():
            raise FrklException(
                msg=f"Can't load target config '{path}'", reason="File does not exist"
            )

        try:
            yaml = YAML()
            content = yaml.load(path)
        except Exception as e:
            raise FrklException(
                msg=f"Can't load target config '{path.as_posix()}'.",
                reason="File content not valid yaml.",
                parent=e,
            )

        if not isinstance(content, collections.abc.MutableMapping):
            raise FrklException(
                msg=f"Can't load target config '{path.as_posix()}'.",
                reason="File content must be dictionary.",
            )

        return content

    def _create_from_dict(
        self, handler_type: HANDLER_TYPE, **config: Mapping[str, Any]
    ) -> EventHandler:
        """Create a target from a dict.

        Args:
            config: the configuration for the target

        Returns:
            EventTarget: the target object
        """

        _config = dict(config)
        target_type: str = _config.pop("type", None)  # type: ignore
        if target_type is None:

            if handler_type == HANDLER_TYPE.source:
                available_handlers = self.available_sources
                type_str = "source"
            else:
                available_handlers = self.available_targets
                type_str = "target"

            raise FrklException(
                msg=f"Can't create {type_str}.",
                reason="No 'type' key specified.",
                solution=f"Add a key 'type' with a value from: {', '.join(available_handlers)}",
            )

        if handler_type == HANDLER_TYPE.source:

            handler_cls = self._source_plugin_manager.get_plugin(
                target_type, raise_exception=True
            )

        else:

            handler_cls = self._target_plugin_manager.get_plugin(
                target_type, raise_exception=True
            )

        handler_obj = handler_cls(**_config)
        return handler_obj

    @property
    def available_sources(self) -> Iterable[str]:
        return self._source_plugin_manager.plugin_names

    @property
    def available_targets(self) -> Iterable[str]:
        return self._target_plugin_manager.plugin_names

    def create(
        self, handler_type: HANDLER_TYPE, config: Union[str, Mapping[str, Any]]
    ) -> EventHandler:

        if isinstance(config, str):
            if os.path.isfile(os.path.realpath(os.path.expanduser(config))):
                config = self._read_config_file(config)
            config = {"type": config}
        elif isinstance(config, collections.abc.Mapping):
            if "type" not in config.keys():
                raise ValueError(
                    f"Invalid handler config. Missing 'type' key: {config}"
                )
        else:
            raise TypeError(
                f"Invalid handler config. Config must be string or mapping, not '{type(config)}'"
            )

        handler_obj = self._create_from_dict(handler_type=handler_type, **config)
        return handler_obj

    def create_source(self, config: Union[str, Mapping[str, Any]]) -> EventSource:

        return self.create(HANDLER_TYPE.source, config)  # type: ignore

    def create_target(self, config: Union[str, Mapping[str, Any]]) -> EventTarget:

        return self.create(HANDLER_TYPE.target, config)  # type: ignore

    def create_event_management(
        self,
        sources: Union[
            EventSource,
            str,
            Mapping[str, Any],
            Iterable[Union[EventSource, str, Mapping[str, Any]]],
        ],
        targets: Union[
            EventTarget,
            str,
            Mapping[str, Any],
            Iterable[Union[EventTarget, str, Mapping[str, Any]]],
        ],
    ) -> EventManagement:

        source_objs: List[EventSource] = []
        target_objs: List[EventTarget] = []

        for sc in ensure_iterable(sources):  # type: ignore
            if not isinstance_or_subclass(sc, EventSource):
                s = self.create_source(sc)
            else:
                s = sc
            source_objs.append(s)

        for tc in ensure_iterable(targets):  # type: ignore
            if not isinstance_or_subclass(tc, EventTarget):
                t = self.create_target(tc)
            else:
                t = tc
            target_objs.append(t)

        event_management = EventManagement(sources=source_objs, targets=target_objs)
        return event_management
