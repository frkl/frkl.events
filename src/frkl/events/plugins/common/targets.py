# -*- coding: utf-8 -*-
from typing import List

from frkl.events.event import Event
from frkl.events.handlers import EventTarget


class CollectorEventTarget(EventTarget):
    """Target to collect results.

    This is mainly used in testing, but can also be used to hold test results in memory for other reasons.
    """

    def __init__(self):

        self._results: List[Event] = []

        super().__init__()

    async def write_events(self, *events: Event) -> None:

        self._results.extend(events)

    @property
    def results(self) -> List[Event]:
        return self._results
