# -*- coding: utf-8 -*-
from typing import AsyncIterator

import anyio
from frkl.events.event import Event, TestEvent
from frkl.events.handlers import EventSource


class DummySource(EventSource):
    def __init__(self):

        super().__init__()

    async def start_source(self) -> AsyncIterator[Event]:  # type: ignore  # not sure why mypy complains here...;
        i = 0
        while True:
            i += 1
            yield TestEvent(f"path_{i}")

            await anyio.sleep(1)

        return


class QueueSource(EventSource):
    def __init__(self):

        self._queue = anyio.create_queue(0)
        super().__init__()

    async def add_event(self, event: Event):

        await self._queue.put(event)

    async def start_source(self) -> AsyncIterator[Event]:

        while True:
            event = await self._queue.get()
            yield event
