# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs


frkl_events_app_dirs = AppDirs("frkl_events", "frkl")

if not hasattr(sys, "frozen"):
    FRKL_EVENTS_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `frkl_events` module."""
else:
    FRKL_EVENTS_MODULE_BASE_FOLDER = os.path.join(
        sys._MEIPASS, "frkl_events"  # type: ignore
    )
    """Marker to indicate the base folder for the `frkl_events` module."""

FRKL_EVENTS_RESOURCES_FOLDER = os.path.join(FRKL_EVENTS_MODULE_BASE_FOLDER, "resources")
