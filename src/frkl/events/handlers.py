# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod
from typing import AsyncIterator, Iterable, Optional

from frkl.common.strings import generate_valid_identifier
from frkl.events.event import Event


class EventHandler(metaclass=ABCMeta):
    def __init__(self, *args, **kwargs):

        self._id: Optional[str] = None

    @property
    def id(self) -> str:

        """The internal id of the source.

        Should be unique across the application invocation.
        """

        if self._id is None:
            self._id = self.get_id()
        return self._id

    def get_id(self) -> str:

        return generate_valid_identifier(length_without_prefix=8)

    def __repr__(self):
        id_str = ""
        if self._id:
            id_str = f"id={self._id}"
        return f"{self.__class__.__name__}({id_str})"


class EventSource(EventHandler):
    """A check source is an object that emits 'CheckResult's.

    The main reason to have an abstract base class here is to enable very flexible plumbing of sources and targets,
    as well as share code for configuration, as well as enable easy addition of new source types.
    """

    async def connect_source(self) -> None:
        """Connect the source."""
        pass

    async def disconnect_source(self) -> None:
        """Disconnect the source."""
        pass

    @abstractmethod
    def start_source(self) -> AsyncIterator[Event]:
        """Start the source.

        This should async-yield Event items.
        """
        pass

    async def stop_source(self) -> Optional[Iterable[Event]]:
        """Stop the source.

        Should return (if necessary) any check result items that haven't been 'yielded' by 'start'.
        """
        return None


class EventTarget(EventHandler):
    """A check target is an object that consumes 'CheckResult's.

    The main reason to have an abstract base class here is to enable very flexible plumbing of sources and targets,
    as well as share code for configuration, as well as enable easy addition of new source types.
    """

    async def connect(self) -> None:
        """Connect the target."""
        pass

    async def disconnect(self) -> None:
        """Disconnect the target."""
        pass

    # @abstractmethod
    # def get_target_schema(self) -> Mapping[str, Any]:
    #     """Return the event schema this target accepts."""
    #
    #     pass

    @abstractmethod
    async def write_events(self, *events: Event) -> None:
        """Write a result to this target."""
        pass
